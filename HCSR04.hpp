#ifndef HCSR04_HPP_
#define HCSR04_HPP_

#include <wiringPi.h>
#include <stdint.h>

class HCSR04
{
  private:
    uint8_t triggerPin;
    uint8_t echoPin;

  public:
    HCSR04();
    virtual ~HCSR04();

    void setTriggerPin(uint8_t triggerPin);
    void setEchoPin(uint8_t echoPin);
    float measure();
};

#endif /* HCSR04_HPP_ */
