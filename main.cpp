#include "HCSR04.hpp"
#include <iomanip>
#include <iostream>

int main(int argc, char **argv)
{
  wiringPiSetup();

  HCSR04 sensor;
  sensor.setTriggerPin(0);
  sensor.setEchoPin(1);

  while (true)
  {
    std::cout << "Distance: " << std::fixed << std::setprecision(2)
        << sensor.measure() << " cm" << std::endl;

    // Sleep 1s
    delay(1000);
  }

  return 0;
}
